<?php
namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Employee;
use Doctrine\Common\Persistence\ObjectManager;

class LuckyController extends AbstractController
{
    /**
     *
     * @var ObjectManager
     */
    protected $objectManager;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @param ObjectManager $objectManager
     */
    //     public function __construct(ObjectManager $objectManager)
    //     {
    //         $this->objectManager = $objectManager;
    //     }
    
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * @Route("/lucky/number/{max}", name="app_lucky_number")
     */
    public function number($max)
    {
        // Init repository
        $employeeRepository = $this->objectManager
        ->getRepository(Employee::class);
        
        // Get data from database
        $employee = $employeeRepository->find($max);
        
        // Log request
        $this->logger->info('ID REQUESTED: ' . $max);
        
        // Return result
        return new Response($employee->getSalary() + $employee->getBonus());
    }
}