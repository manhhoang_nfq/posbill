<?php
/**
 * File description
 *
 * PHP version 7
 *
 * @category  PHP
 * @package   App\Http\Controllers\API
 * @author    Manh Hoang <manh.hoang@nfq.asia>
 * @copyright NFQ ASIA
 * @link      https://www.facebook.com/nfq.asia
 */

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;

/**
 * This class use to do some thing
 * Class NewClass
 * @package App\Http\Controllers\API
 */
class NewClass
{
    public function __construct(User $user)
    {
    }

    /**
     * @param Request $request
     */
    public function getSomething(Request $request)
    {
    }
}
