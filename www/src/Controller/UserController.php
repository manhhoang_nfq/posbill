<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;


class  UserController extends AbstractController
{
    private $objectManager;

    public function  __construct(ObjectManager $manager)
    {
        $this->objectManager = $manager;
    }

    public  function  index() {
        //$token = $this->container->get('security.token_storage')->getToken();
        $userResponsitory = $this->objectManager->getRepository(User::class);
        $user = $userResponsitory->findAll();
        $something = 'ok';
        // var_dump($users);
        return new Response(json_encode($user));
    }

}