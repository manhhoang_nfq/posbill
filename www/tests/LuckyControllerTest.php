<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use App\Entity\Employee;

class LuckyControllerTest extends WebTestCase
{
    /**
     * 
     */
    public function testCallDB()
    {   
        $client =  static::createClient(/*array(
                'environment' => 'test',
                'debug'       => true,
            )*/);
        
        $client->request('GET', '/lucky/number/1');
        
        //$this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('2001', $client->getResponse()->getContent());
    }
    
    /**
     * 
     */
    public function testMock(){
        $employee = new Employee();
        $employee->setSalary(1000);
        $employee->setBonus(1100);

        // Now, mock the repository so it returns the mock of the employee
        $employeeRepository = $this->createMock(ObjectRepository::class);
        // use getMock() on PHPUnit 5.3 or below
        // $employeeRepository = $this->getMock(ObjectRepository::class);
        $employeeRepository->expects($this->any())
            ->method('find')
            ->willReturn($employee);

        // Last, mock the EntityManager to return the mock of the repository
        $objectManager = $this->createMock(ObjectManager::class);
        // use getMock() on PHPUnit 5.3 or below
        // $objectManager = $this->getMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($employeeRepository);
        
        $controller = new \App\Controller\LuckyController($objectManager);
        
        $result = $controller->number(1);
        $this->assertContains('2100', "$result");
        $this->assertTrue(true);
    }
}
