<?php

namespace App\Tests;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;

class UserControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $user = new User('someone');
        $user->setPassword("password");

        $employeeRepository = $this->createMock(ObjectRepository::class);
        $employeeRepository->expects($this->any())
            ->method('findAll')
            ->willReturn($user);

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($employeeRepository);

        $client =  static::createClient();

        $client->request('GET', '/users');

        $this->assertEquals(500, $client->getResponse()->getStatusCode());

        //$this->assertTrue(true);
    }
}
